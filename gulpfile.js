'use strict';
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    less = require('gulp-less'),
    cssnano = require('gulp-cssnano'),
    mainBowerFiles = require('main-bower-files'),
    uglify = require('gulp-uglify'),
    browserSync = require("browser-sync"),
    filter = require('gulp-filter'),
    imagemin = require('gulp-imagemin'),
    plugins = require("gulp-load-plugins")({
        pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
        replaceString: /\bgulp[\-.]/
    }),
    reload = browserSync.reload,
    uncss = require('gulp-uncss'),
    server = require('gulp-express')
;


var path = {
    build: {
        html: './build',
        js: './build/js/',
        css: './build/css/',
        img: './build/images/',
        fonts: './build/fonts/',
        vendors: {
            css: './build/css/',
            js: './build/js/',
            fonts: './build/fonts/'
        }
    },
    src: {
        html: './src/**/*.html',
        js: './src/app/**/*.js',
        styles: './src/styles/*.less',
        img: './src/images/**/*.*',
        fonts: './src/fonts/**/*.*',
        vendors: mainBowerFiles()


    },
    watch: {
        html: './src/**/*.html',
        js: './src/app/**/*.js',
        styles: './src/styles/*.less',
        img: './src/img/**/*.*',
        fonts: './src/fonts/**/*.*'

    }
};

gulp.task('webserver', function () {
    browserSync({
        server: {
            baseDir: "./build"
        },
        tunnel: true,
        host: 'localhost',
        port: 9000
    })
});

gulp.task('styles:build', function() {
    return gulp.src(path.src.styles)
        .pipe(less())
        .pipe(autoprefixer())
        .pipe(cssnano())
        .pipe(concat('index.css'))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}))
        ;
});

gulp.task('js:build', function () {
    return gulp.src(path.src.js)
        .pipe(concat('index.js'))
        // .pipe(uglify())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}))
        ;
});

gulp.task('server:build', function () {
    return gulp.src(path.src.server)
        .pipe(gulp.dest(path.build.server))
        ;
});

gulp.task('node_modules:build', function () {
    return gulp.src(path.src.node_modules)
        .pipe(gulp.dest(path.build.node_modules))
        ;
});

gulp.task('html:build', function () {
    return gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}))
        ;
});

gulp.task('fonts:build', function () {
    return gulp.src(path.src.fonts)
        .pipe(plugins.filter('**/*.{eot,svg,ttf,woff,woff2}', {restore: true}))
        .pipe(gulp.dest(path.build.fonts))
        .pipe(reload({stream: true}))
        ;
});

gulp.task('images:build', function () {
    return gulp.src(path.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}))
        ;
});

gulp.task('vendors:build', function() {
    gulp.src(path.src.vendors)
        .pipe(plugins.filter('**/*.js', {restore: true}))
        .pipe(concat('vendors.js'))
        .pipe(uglify())
        .pipe(gulp.dest(path.build.vendors.js))
        ;

    gulp.src(path.src.vendors)
        .pipe(plugins.filter('**/*.less', {restore: true}))
        .pipe(less())
        .pipe(cssnano())
        .pipe(concat('vendors.css'))
        .pipe(gulp.dest(path.build.vendors.css))
        ;

    gulp.src(path.src.vendors)
        .pipe(plugins.filter('**/*.css'))
        .pipe(cssnano())
        // .pipe(uncss({
        //     html: ['./src/index.html']
        // }))
        .pipe(concat('vendors.css'))
        .pipe(gulp.dest(path.build.vendors.css))
        ;

    gulp.src(path.src.vendors)
        .pipe(plugins.filter('**/*.{eot,svg,ttf,woff,woff2}', {restore: true}))
        .pipe(gulp.dest(path.build.vendors.fonts))
        ;

});

gulp.task('build', [
    'styles:build',
    'js:build',
    'html:build',
    'vendors:build',
    'fonts:build',
    'images:build'
]);

gulp.task('start', [
    'build',
    'webserver',
    'watch'
]);

gulp.task('watch', function () {
    gulp.watch(path.src.styles, ['styles:build']),
    gulp.watch(path.src.js,     ['js:build']),
    gulp.watch(path.src.html,   ['html:build']),
    gulp.watch(path.src.fonts,  ['fonts:build']),
    gulp.watch(path.src.img,    ['images:build'])
});

