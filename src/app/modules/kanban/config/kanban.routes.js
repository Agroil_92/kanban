(function () {
    var app = angular.module('app');
    app.config(function($stateProvider, $locationProvider) {
        $stateProvider
            .state('init', {
                url: '/',
                templateUrl: 'app/modules/kanban/views/list.html',
                controller: 'kanbanCtrl as vm'
            })
        ;
        $locationProvider.html5Mode(true);
    });
}());
