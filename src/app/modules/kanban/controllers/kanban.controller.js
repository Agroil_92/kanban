(function () {
    var app = angular.module('app');
    app.controller('kanbanCtrl',
        function () {
            const vm = this;
            vm.models = {
                selected: null,
                lists: {
                    "toDo": [],
                    "plan": [],
                    "develop": [],
                    "test": [],
                    "deploy": [],
                    "done": []
                }
            };

            vm.remove = (item, arr) => arr.splice(arr.indexOf(item), 1);

            vm.postTask = task =>
                {
                    if (task.text && task.radio) {
                        vm.models.lists.toDo.push({label: task.text, radio: task.radio});
                        vm.task.text = ''
                    }
                }
    });
}());

